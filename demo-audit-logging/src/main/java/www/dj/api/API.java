package www.dj.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import www.dj.dao.DAO;
import www.dj.entity.User;

@RestController
@RequestMapping(value = "/audit-logging", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@Api(value = "Audit Logging POC", description = "Audit Logging POC using Hibernate Envers as well as DB Triggers", tags = { "Audit Logging POC" })
public class API {

	@Autowired
	private DAO dao;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation("Creates new primary entity")
	public void create(@RequestBody final User user) {
		dao.create(user);
		log.info("Record created");
	}

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation("Retrieves primary entity")
	public User get(@RequestHeader(value = "id", required = true) final int id) {
		return dao.get(id);
	}

	@PutMapping
	@ResponseStatus(HttpStatus.CREATED)
	@ApiOperation("Edits existing entity")
	public void edit(@RequestBody final User user) {
		dao.edit(user);
		log.info("Record edited");
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@ApiOperation("Deletes existing entity")
	public void delete(@PathVariable final int id) {
		dao.delete(id);
		log.info("Record deleted");
	}

	@GetMapping("/audit")
	@ApiOperation("Retrieves change history for primary entity")
	public List<User> getChangeHistory(@RequestHeader("id") int id) {
		return dao.getUserAudit(id);
	}
}
