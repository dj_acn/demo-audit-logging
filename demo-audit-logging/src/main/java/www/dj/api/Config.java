package www.dj.api;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Config {

	@Value("${swagger.enabled:true}")
	private boolean swaggerEnabled;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).enable(swaggerEnabled).select().apis(RequestHandlerSelectors.basePackage("www.dj")).paths(PathSelectors.any()).build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("Audit Logging Test API", "Audit Logging Test API", "1.0", "Terms of service", new Contact("Dipanjan", "http://www.google.com", "dipanjan.chakrabarti@accenture.com"), "License of API", "API license URL", new ArrayList<>());
	}

	@Bean
	public EntityManager entityManager() {

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("UsersDB");
		return factory.createEntityManager();
	}
}
