package www.dj.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import www.dj.entity.User;

@Component
public class DAO {

	@Autowired
	private EntityManager entityManager;

	public void create(final User user) {
		entityManager.getTransaction().begin();
		entityManager.persist(user);
		entityManager.getTransaction().commit();
	}

	public void edit(final User user) {
		entityManager.getTransaction().begin();
		entityManager.persist(entityManager.contains(user) ? user : entityManager.merge(user));
		entityManager.getTransaction().commit();
	}

	public void delete(final int id) {
		entityManager.getTransaction().begin();
		User user = new User();
		user.setId(id);
		entityManager.remove(entityManager.contains(user) ? user : entityManager.merge(user));
		entityManager.getTransaction().commit();
	}

	public User get(final int id) {
		return entityManager.find(User.class, id);
	}

	public List<User> getUserAudit(final int id) {
		AuditReader auditReader = AuditReaderFactory.get(entityManager);
		AuditQuery auditQuery = auditReader.createQuery().forRevisionsOfEntity(User.class, false, true);
		auditQuery.add(AuditEntity.id().eq(id));
		return (List<User>) auditQuery.getResultList();
	}
}
