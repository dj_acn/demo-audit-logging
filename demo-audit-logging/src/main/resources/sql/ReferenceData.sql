-- create database envers;

-- Create primary entity, auto generate ID and timestamps
drop table if exists envers.reference_data;
create table envers.reference_data (
	id int(11) not null auto_increment,
	create_user varchar(10) not null,
	create_ts timestamp default current_timestamp,
	update_user varchar(10) not null,
	update_ts timestamp default current_timestamp on update current_timestamp,
	data_classification varchar(20) not null,
	code varchar(50) not null,
	description varchar(255) not null,
	effective_date date,
	expiry_date date,
	primary key (id)
);

-- Create change history table for primary entity.
drop table if exists envers.reference_data_audit;
create table envers.reference_data_audit (
	id int(11) not null auto_increment,
	reference_data_id int(11) not null,
	action_type varchar(10) not null,
	action_user varchar(10) not null,
	action_ts timestamp not null,
	data_classification varchar(20),
	code varchar(50),
	description varchar(255),
	effective_date date,
	expiry_date date,
	primary key (id)
);

-- Create an unique key to prevent duplicate entries
create unique index reference_data_index ON envers.reference_data (data_classification, code, effective_date);

-- Create Post Insert trigger
drop trigger if exists envers.reference_data_pit;
create trigger envers.reference_data_pit
after insert on reference_data
for each row insert into
envers.reference_data_audit (action_type, action_user, action_ts, reference_data_id, data_classification, code, description, effective_date, expiry_date)
values ('CREATED', NEW.update_user, NEW.update_ts, NEW.id, NEW.data_classification, NEW.code, NEW.description, NEW.effective_date, NEW.expiry_date);

-- Create Post Update trigger
drop trigger if exists envers.reference_data_put;
create trigger envers.reference_data_put
after update on reference_data
for each row insert into
envers.reference_data_audit (action_type, action_user, action_ts, reference_data_id, data_classification, code, description, effective_date, expiry_date)
values ('EDITED', NEW.update_user, NEW.update_ts, NEW.id, NEW.data_classification, NEW.code, NEW.description, NEW.effective_date, NEW.expiry_date);

-- Create Post Delete trigger
drop trigger if exists envers.reference_data_pdt;
create trigger envers.reference_data_pdt
after delete on reference_data
for each row insert into
envers.reference_data_audit (action_type, action_user, action_ts, reference_data_id, data_classification)
values ('DELETED', USER(), OLD.update_ts, OLD.id, OLD.data_classification);

-- Insert sample value for primary entity, this should create audit entry with action "CREATED"
insert into envers.reference_data (create_user, update_user, data_classification, code, description, effective_date) values 
("F658604", "F658604", "DOMAIN", "KPCC", "Billing Domain Group", DATE '2019-12-24'),
("F658604", "F658604", "DOMAIN", "BILLING", "Billing Domain Group", DATE '2019-12-24'),
("F658604", "F658604", "APPLICATION", "TPST_ACCUMS", "Tapestry Accumulations", DATE '2019-12-24'),
("F658604", "F658604", "SELECTOR", "AHC_INBOUND_BUCKET_ID_MAPPING", "ID Mapping", DATE '2019-12-24'),
("F658604", "F658604", "SELECTOR", "EPIC_WEB_SERVICE_GENDER", "Gender", DATE '2019-12-24');

-- Update existing entity, this should create audit entry with action "EDITED"
update envers.reference_data set update_user="I331790", description = "Member Connect" where id = 1;

-- Delete existing entity, this should create audit entry with action "DELETED"
delete from reference_data where id = 5;

-- Verify audit table has all information captured correctly
select * from envers.reference_data_audit order by id;