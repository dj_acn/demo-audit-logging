-- create database envers;

-- Create primary entity, auto generate ID and timestamps
drop table if exists envers.crosswalk;
create table envers.crosswalk (
	id int(11) not null auto_increment,
	create_user varchar(10) not null,
	create_ts timestamp default current_timestamp,
	update_user varchar(10) not null,
	update_ts timestamp default current_timestamp on update current_timestamp,
	region varchar(3) not null,
	domain_group varchar(50) not null,
	application varchar(50) not null,
	selector varchar(50) not null,
	source_value varchar(100) not null,
	destination_value varchar(100) not null,
	description varchar(255),
	effective_date date not null,
	expiry_date date,
	active_ind varchar(1) default 'N',
	default_ind varchar(1),
	status_change_reason varchar(100),
	seq_xwalk_id int(11),
	primary key (id)
);

-- Create an unique key to prevent duplicate entries
create unique index crosswalk_index ON envers.crosswalk (region, domain_group, application, selector, source_value, destination_value, effective_date);

-- Create change history table for primary entity.
drop table if exists envers.crosswalk_audit;
create table envers.crosswalk_audit (
	id int(11) not null auto_increment,
	action_type varchar(10) not null,
	action_user varchar(10) not null,
	action_ts timestamp not null,
	xwalk_id int(11) not null,
	region varchar(3),
	domain_group varchar(50),
	application varchar(50),
	selector varchar(50),
	source_value varchar(100),
	destination_value varchar(100),
	description varchar(255),
	effective_date date,
	expiry_date date,
	active_ind varchar(1),
	default_ind varchar(1),
	status_change_reason varchar(100),
	seq_xwalk_id int(11),
	primary key (id)
);

-- Create Post Insert trigger
drop trigger if exists envers.crosswalk_pit;
create trigger envers.crosswalk_pit
after insert on crosswalk
for each row insert into
envers.crosswalk_audit (action_type, action_user, action_ts, xwalk_id, region, domain_group, application, selector, source_value, destination_value, description, effective_date, expiry_date, active_ind, default_ind, status_change_reason, seq_xwalk_id)
values ('CREATED', NEW.update_user, NEW.update_ts, NEW.id, NEW.region, NEW.domain_group, NEW.application, NEW.selector, NEW.source_value, NEW.destination_value, NEW.description, NEW.effective_date, NEW.expiry_date, NEW.active_ind, NEW.default_ind, NEW.status_change_reason, NEW.seq_xwalk_id);

-- Create Post Update trigger
drop trigger if exists envers.crosswalk_put;
create trigger envers.crosswalk_put
after update on crosswalk
for each row insert into
envers.crosswalk_audit (action_type, action_user, action_ts, xwalk_id, region, domain_group, application, selector, source_value, destination_value, description, effective_date, expiry_date, active_ind, default_ind, status_change_reason, seq_xwalk_id)
values ('EDITED', NEW.update_user, NEW.update_ts, NEW.id, NEW.region, NEW.domain_group, NEW.application, NEW.selector, NEW.source_value, NEW.destination_value, NEW.description, NEW.effective_date, NEW.expiry_date, NEW.active_ind, NEW.default_ind, NEW.status_change_reason, NEW.seq_xwalk_id);

-- Create Post Delete trigger
drop trigger if exists envers.crosswalk_pdt;
create trigger envers.crosswalk_pdt
after delete on crosswalk
for each row insert into
envers.crosswalk_audit (action_type, action_user, action_ts, xwalk_id)
values ('DELETED', USER(), OLD.update_ts, OLD.id);

-- Insert sample value for primary entity, this should create audit entry with action "CREATED"
insert into envers.crosswalk (region, create_user, update_user, domain_group, application, selector, source_value, destination_value, description, effective_date)
values ("HAW", "F658604", "F658604", "KPCC", "TPST_ACCUMS", "EPIC_WEB_SERVICE_GENDER", "0", "M", "MALE", date '2019-12-24'),
("HAW", "F658604", "F658604", "KPCC", "TPST_ACCUMS", "EPIC_WEB_SERVICE_GENDER", "1", "F", "FEMALE", date '2019-12-24'),
("HAW", "F658604", "F658604", "KPCC", "TPST_ACCUMS", "EPIC_WEB_SERVICE_GENDER", "2", "T", "TRANSGENDER", date '2019-12-24'),
("HAW", "F658604", "F658604", "KPCC", "TPST_ACCUMS", "EPIC_WEB_SERVICE_GENDER", "3", "U", "UNDISCLOSED", date '2019-12-24');

-- Update existing entity, this should create audit entry with action "EDITED"
update crosswalk set update_user="I331790", expiry_date = DATE '2025-12-31', active_ind='Y' where id=1;

-- Delete existing entity, this should create audit entry with action "DELETED"
delete from crosswalk where id=1;

-- Verify audit table has all information captured correctly
select * from crosswalk_audit order by xwalk_id, action_ts;